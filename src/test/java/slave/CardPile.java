package slave;

import java.util.Arrays;

public class CardPile {
	private Card[] cards;

	public CardPile(Card[] cards) {
		this.cards = cards;
	}

	public Card[] getCards() {
		return cards;
	}

	public boolean canBePlacedOnTopOf(CardPile otherPile) {
		if (otherPile == null)
			return true;
		if (this.cards.length % 2 != otherPile.cards.length % 2)
			return false;
		if (this.cards.length > otherPile.cards.length)
			return true;
		if (this.cards.length < otherPile.cards.length)
			return false;
		return this.getBiggestCardInThisPile().isBiggerThan(otherPile.getBiggestCardInThisPile());
	}

	public Card getBiggestCardInThisPile() {
		Card ret = cards[0];
		for (int i = 1; i < cards.length; i++) {
			if (cards[i].isBiggerThan(ret))
				ret = cards[i];
		}
		return ret;
	}

	public String toString() {
		return Arrays.toString(cards);
	}
}
