package slave;

public class Card {
	int rank, suit;

	public Card(int rank, int suit) {
		if (rank < 1 || 13 < rank) {
			rank = 1;
		}
		if (suit < 1 || 4 < suit) {
			suit = 1;
		}
		this.suit = suit;
		this.rank = rank;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		if (rank < 1 || 13 < rank) {
			rank = 1;
		}
		this.rank = rank;
	}

	public int getSuit() {
		return suit;
	}

	public void setSuit(int suit) {
		if (suit < 1 || 4 < suit) {
			suit = 1;
		}
		this.suit = suit;
	}

	private int getScore() {
		int[] rankScore = { 555, 11, 12, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		int[] suitScore = { 666, 0, 1, 2, 3 };
		return rankScore[rank] * 4 + suitScore[suit];
	}

	public boolean isBiggerThan(Card card) {
		return this.getScore() > card.getScore();
	}

	public String toString() {
		String[] rankStr = { "GG", "A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K" };
		String[] suitStr = { "EZ", "Club", "Diamond", "Heart", "Spade" };
		return suitStr[suit]+" "+rankStr[rank];
	}

}
