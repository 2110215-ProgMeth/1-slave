package slave;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.stream.Collectors;

public class Utility {
	public static int random(int start, int end) {
		Random rand = new Random();
		return (int) (rand.nextInt(end - start + 1) + start);
	}

	public static int random(int start, int end, long seed) {
		Random rand = new Random();
		rand.setSeed(seed);
		return (int) (rand.nextInt(end - start + 1) + start);
	}

	public static ArrayList<CardPile> generatePossibleCardPile(ArrayList<Card> hand) {
		ArrayList<CardPile> ret = new ArrayList<>();
		hand.stream().collect(Collectors.groupingBy(x -> x.getRank())).forEach((ii, x) -> {
			for(int i=0;i<x.size();i++){
				Card c1=x.get(i);
				ret.add(new CardPile(new Card[]{c1}));
				for(int j=i+1;j<x.size();j++){
					Card c2=x.get(j);
					ret.add(new CardPile(new Card[]{c1,c2}));
					for(int k=j+1;k<x.size();k++){
						Card c3=x.get(k);
						ret.add(new CardPile(new Card[]{c1,c2,c3}));
					}
				}
			}
			if(x.size()==4){
				Card[] c=new Card[4];
				x.toArray(c);
				ret.add(new CardPile(c));
			}
		});
		ret.stream().forEach(x->Arrays.sort(x.getCards(),(y,z)->z.isBiggerThan(y)?1:-1));
		Collections.sort(ret, (x, y) -> {
			if(x.getCards().length!=y.getCards().length){
				return y.getCards().length-x.getCards().length;
			}
			else{
				for(int i=0;i<x.getCards().length;i++){
					if(x.getCards()[i].isBiggerThan(y.getCards()[i])){
						return -1;
					}
					else if(y.getCards()[i].isBiggerThan(x.getCards()[i])){
						return 1;
					}
				}
				return 0;
			}
		});
//		for(CardPile cp:ret)System.out.println(cp);
		return ret;
	}
}
