package slave;

import java.util.ArrayList;

public class GameManager {
	static ArrayList<Player> players = new ArrayList<>();
	static int now = 0;

	public static void nextPlayer() {
		now++;
		now %= players.size();
	}

	public static boolean isAllPlayerWin() {
		return players.stream().filter(x->!x.win()).count()==0;
	}

	public static void main(String[] args) {
		players.add(new Player("Alice"));
		players.add(new Player("Bill"));
		players.add(new Player("Cain"));
		players.add(new Player("Doug"));
		Deck deck = new Deck();
		deck.shuffle();
		while (deck.getDeckSize() != 0) {
			players.get(now).addCardToHand(deck.deal());
			nextPlayer();
		}
		Field field = new Field();
		now = 0;
		while (true) {
			if (players.stream().filter(x -> !x.isPass()).count() == 0) {
				System.out.println("All players declare \"PASS\"");
				field.clearField();
				players.stream().forEach(x -> x.setPass(false));
				now = 0;
			}

			if (players.get(now).win()) {
				System.out.println(players.get(now).getName() + " passes with empty hand");
				players.get(now).setPass(true);
				players.get(now).won(true);
			}
			else if(!players.get(now).isPass() && Utility.generatePossibleCardPile(players.get(now).getCardsInHand()).stream().filter(x->x.canBePlacedOnTopOf(field.getTopPile())).count()!=0){
				players.get(now).play(field);
				System.out.println(players.get(now).getName() + " plays " + field.getTopPile().toString());
			}
			else {
				System.out.println(players.get(now).getName() + " passes");
				players.get(now).setPass(true);
			}

			if (isAllPlayerWin()) {
				System.out.println(players.get(now).getName() + " is a SLAVE!");
				break;
			}
			nextPlayer();
		}

	}
}
