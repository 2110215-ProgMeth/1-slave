package slave;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Player {
	private String name;
	private boolean pass = false;
	private ArrayList<Card> cardsInHand;
	private boolean won = false;
	public Player(String name) {
		this.name = name;
		cardsInHand = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPass() {
		return pass;
	}

	public void setPass(boolean pass) {
		this.pass = pass;
	}

	public ArrayList<Card> getCardsInHand() {
		return cardsInHand;
	}

	public void addCardToHand(Card card) {
		cardsInHand.add(card);
	}

	public void play(Field field) {
		ArrayList<CardPile> cps = Utility.generatePossibleCardPile(cardsInHand).stream()
				.filter((x) -> x.canBePlacedOnTopOf(field.getTopPile()))
				.collect(Collectors.toCollection(ArrayList::new));
		if (cps.size() != 0) {
			CardPile cp = cps.get(cps.size() - 1);
			field.setTopPile(cp);
			cardsInHand.removeAll(Arrays.asList(cp.getCards()));
		} else {
			this.setPass(true);
		}
	}

	public boolean win() {
		return cardsInHand.size() == 0;
	}
	public void won(boolean won){
		this.won=won;
	}
	public boolean won(){
		return won;
	}
	
}
