package slave;

public class Deck {
	private int deckCount;
	private Card[] cards;

	public Deck() {
		deckCount = 52;
		cards = new Card[52];
		for (int i = 1; i <= 4; i++) {
			for (int j = 1; j <= 13; j++) {
				cards[(i - 1) * 13 + (j - 1)] = new Card(j, i);
			}
		}
	}

	public void shuffle() {
		for (int i = 0; i < 500; i++) {
			int a = Utility.random(0, deckCount - 1);
			int b = Utility.random(0, deckCount - 1);
			Card t = cards[a];
			cards[a] = cards[b];
			cards[b] = t;
		}
	}
	public void shuffle(long seed){
		for (int i = 0; i < 500; i++) {
			int a = Utility.random(0, deckCount - 1,seed+i);
			int b = Utility.random(0, deckCount - 1,seed+i);
			Card t = cards[a];
			cards[a] = cards[b];
			cards[b] = t;
		}
	}

	public Card deal() {
		return cards[--deckCount];
	}

	public int getDeckSize() {
		return deckCount;
	}
}
